# -*- coding: utf-8 -*-'''

#########################################
# CI3715 Traductores e Interpretadores  #
# PascalUSB                             #
# Etapa 2-AST                           #
# Fernando Gonzalez 08-10464            #
# Honorio Rodriguez 09-11023            #
#########################################

import re

global alcance
alcance = 1

class Expression:
    pass

###############################################################################
#									INICIO				   					  #
###############################################################################

# Clase encargada encargada de mostrar la sintaxis de la estructura de un programa
# en el lenguaje PascalUSB.

class Bloque (Expression):
	def __init__ (self,Declare, Begin, PostCondicion):
		self.Declare = Declare
		self.Begin = Begin
		self.PostCondicion = PostCondicion
	
	def __str__ (self):
		retorno = ""
		retorno += "\n"
		global alcance
		if alcance > 0:
			retorno += "\n"+"\n"
			retorno += "\t"+"\t"+"BLOQUE"
		if self.Declare != None:
			retorno += ""

		if  1 < len(self.Begin):
			retorno += "SECUENCIACION"
			retorno += "\n"

		for b in loqsea	:
			retorno += str(b)
			alcance = alcance + 1
		
		if self.PostCondicion != None:
			retorno += ""		

		retorno += "\n"
		return retorno

###############################################################################
#								INSTRUCCIONES				   				  #
###############################################################################

class Asignacion (Expression):
    # Clase que muestra la sintaxis de la instruccion asignacion de PascalUSB

    def __init__ (self, ident, expr):
        self.type = "ASIGNACION"
        self.ident = ident
        self.expr = expr

    def __str__ (self):
		espacio ="    "
		retorno = ""
		retorno += "\n"
		retorno += espacio + str(self.type)
		retorno += "\n"
		if not(re.match('([A-Za-z][A-Za-z0-9_]*(?:_+[A-Za-z0-9_]+)*)',str(self.ident))) :
			retorno += espacio + espacio + "- contenedor: " + str(self.ident)
		else :
			retorno += espacio + espacio + "- contenedor: " + Variable(self.ident)
		retorno += "\n"
		if  not(re.match('([A-Za-z][A-Za-z0-9_]*(?:_+[A-Za-z0-9_]+)*)',str(self.ident))) :
			retorno += espacio + espacio + "- expresion: " + str(self.expr)
		else :
			retorno += espacio + espacio + "- expresion: " + Variable(self.expr)
		return retorno    

class Condicional (Expression):
    # Clase que muestra la sintaxis de la instruccion condicional de PascalUSB

    def __init__ (self, expr, listCondIf, listCondElse = None):
		self.type = "CONDICIONAL"
		self.expr = expr
		self.listCondIf = listCondIf
		self.listCondElse = listCondElse

    def __str__ (self):
		espacio = "    "
		retorno = ""
		retorno += "\n"
		retorno = espacio + str(self.type)
		retorno += "\n"
		retorno += str(self.expr)
		retorno += "\n"
		retorno += espacio + espacio + "- exito : "
		retorno += "\n"
		retorno += "\t" + "\t"+ "CONDICIONAL"
		retorno += "\n"
		
		for inst in self.listCondIf :
			retorno += str(inst)
			retorno += "\n"
		
		if self.listaCondElse != None :
			retorno += espacio + espacio + "-fracaso :"
			retorno += "\n"
			retorno += "\t" + "\t"+ "ELSE"
			retorno += "\n"
			for other in self.listCondElse :
				retorno += str(other)
				retorno += "\n"
				return retorno

class For (Expression):
    def __init__ (self, ident, paso, instr):
        self.type = "ITERACIÓN DETERMINADA"
        self.ident = ident
        self.paso = paso
        self.instr = instr

    def __str__ (self):
		espacio ="    "
		retorno = ""
		retorno += "\n"
		retorno += espacio + str(self.type)
		retorno += "\n"
		retorno += espacio + espacio + "Variable de la iteracion: " 
		if  not(re.match('([A-Za-z][A-Za-z0-9_]*(?:_+[A-Za-z0-9_]+)*)',str(self.ident))) :
			retorno += str(self.ident)
			retorno += "\n"
		else :
			retorno += Variable(self.ident)
			retorno += "\n"
		
		if not (self.paso == None) :
			retorno += "\n"
			retorno += espacio + espacio + "tamaño del paso: " 
			retorno += str(self.paso)

		retorno += "\n"
		retorno += "\n"
		retorno += "\t" + "\t"+ "INSTRUCCIONES DEL FOR"
		retorno += "\n"
		for i in self.instr :
			retorno += str(i)
		return retorno

class While (Expression):
    # Clase que muestra la sintaxis de las instruccion de iteracion indeterminada
    # del lenguaje PascalUSB        
    
    def __init__ (self, expr, instr):
        self.type = "ITERACION INDETERMINADA"
        self.expr = expr
        self.instr = instr

    def __str__(self):
		espacio ="    "
		retorno = ""
		retorno += "\n"
		retorno += espacio + self.type
		retorno += "\n"
		if  not(re.match('([A-Za-z][A-Za-z0-9_]*(?:_+[A-Za-z0-9_]+)*)',str(self.expr))) :
			retorno += espacio + espacio + str(self.expr)
		else :
			retorno += Variable(self.expr)

		retorno += "\n"
		retorno += "\n"
		retorno += "\t" + "\t"+ "INSTRUCCIONES DEL WHILE <------------"
		retorno += "\n"
		for i in self.instr :
			retorno += str(i)
		return retorno    

class Read (Expression):
    # Clase que muestra la sintaxis de la instruccion de lectura de PascalUSB

    def __init__ (self, ident):
        self.type = "READ"
        self.ident = ident
	
	def __str__ (self):
		espacio ="    "
		retorno = ""
		retorno += espacio + str(self.type)
		retorno += "\n"
		if  not(re.match('([A-Za-z][A-Za-z0-9_]*(?:_+[A-Za-z0-9_]+)*)',str(self.ident))) :
			retorno += espacio + espacio + " - identificador: " + str(self.ident)
		else :
			retorno += espacio + espacio + " - identificador: " + Variable(self.ident)
		return retorno

# Clase que muestra la sintaxis de la instruccion de salida del lenguaje PascalUSB
class Print (Expression):
	
	def __init__(self, expre):
		self.type = "PRINT"
		self.expre = expre
	
	def __str__ (self):
		espacio ="    "
		retorno = ""
		retorno += espacio + str(self.type)
		retorno += "\n"
		if not(re.match('([A-Za-z][A-Za-z0-9_]*(?:_+[A-Za-z0-9_]+)*)',str(self.expre))) :
			retorno += espacio + espacio + "- expresion: " + str(self.expre)
		else :
			retorno += espacio + espacio + "- expresion: " + Variable(self.expre)
		return retorno       
    
# Clase que muestra la sintaxis de la instruccion de salida con salto de linea del lenguaje PascalUSB
class Println (Expression):
	def __init__(self, expre):
		self.type = "PRINTLN"
		self.expre = expre

	def __str__ (self):
		espacio ="    "
		retorno = ""
		retorno += espacio + str(self.type)
		retorno += "\n"
		if not(re.match('([A-Za-z][A-Za-z0-9_]*(?:_+[A-Za-z0-9_]+)*)',str(self.expre))) :
			retorno += espacio + espacio + "- expresion: " + str(self.expre)
		else :
			retorno += espacio + espacio + "- expresion: " + Variable(self.expre)
		return retorno      

# Clase que muestra la sintaxis de la instruccion Case
class Case (Expression):
	
	def __init__ (self, expr, listCase):
		self.type = "CASE"
		self.expr = expr
		self.listCondIf = listCase
	
	def __str__ (self):
		espacio = "    "
		retorno = ""
		retorno += "\n"
		retorno = espacio + str(self.type)
		retorno += "\n"
		retorno += str(self.expr)
		retorno += "\n"
		retorno += espacio + espacio + "- exito : "
		retorno += "\n"
		retorno += "\t" + "\t"+ "CASE"
		retorno += "\n" 
		
		for inst in self.listCase :
			retorno += str(inst)
			retorno += "\n"
		
		return retorno

###############################################################################
#						EXPRESIONES ARITMETICAS			   					  #
###############################################################################

# Diccionario de los operandos validos en el lenguaje PascalUSB

binary_symbol = {
                    "+"     : "Suma",
					"-"     : "Resta",
					"*"     : "Multiplicación",
					"/"     : "Division",
					"%"     : "Modulo",
					"and"   : "Conjuncion",
					"or"    : "Disyuncion",
					"not"   : "Negacion",
					"<"     : "Menor",
					"<="    : "Menor o Igual",
					">"     : "Mayor",
					">="    : "Mayor o Igual",
					"=="     : "Igual",
					"/="    : "Desigualdad",
					".."    : "So Forth",
					"<>"    : "Cap",
}

def OperacionUnaria(expresion, operador):

	# Funcion encargada de mostrar la sintaxis de las operaciones unarias
	# en el lenguaje PascalUSB.

	espacio ="    "
	retorno =""
	operador = binary_symbol[operador]
	retorno += (espacio + espacio + espacio + " - operador : " + "'" + operador + "'")
	retorno += "\n"
	retorno += espacio + espacio + espacio + " - expresion : " 
	if not(re.match('([A-Za-z][A-Za-z0-9_]*(?:_+[A-Za-z0-9_]+)*)',str(expresion))) :
		retorno += str(expresion)
	else :
		retorno += Variable(expresion)
	return retorno

def OperacionBinaria (expresion1, operador, expresion2):

	# Funcion encargada de mostrar la sintaxis de las operaciones binarias
	# en el lenguaje PascalUSB.

	espacio ="    "
	retorno =""
	operador = binary_symbol[operador]
	retorno += (espacio + espacio + espacio + "- operacion: " + "'" + operador + "'")
	retorno += "\n"
	retorno += espacio + espacio + espacio + "- operando izquierdo: "
	if  not (re.match('([A-Za-z][A-Za-z0-9_]*(?:_+[A-Za-z0-9_]+)*)',str(expresion1))) :
		retorno += str(expresion1)
	else :
		retorno += Variable(expresion1)
	retorno += "\n"
	retorno += espacio + espacio + espacio + "- operando derecho: " 
	if  not (re.match('([A-Za-z][A-Za-z0-9_]*(?:_+[A-Za-z0-9_]+)*)',str(expresion2))) :
		retorno += str(expresion2)
	else :
		retorno += Variable(expresion2)
	return retorno

def expresionParentesis(expresion):

	# Funcion encargada de mostrar la sintaxis de las expresiones parentizadas
	# en el lenguaje PascalUSB.

	espacio ="    "
	retorno =""
	retorno += (espacio + espacio + espacio +  "- parentesis izquierdo: " +  "'('")
	retorno += "\n"
	retorno += espacio + espacio + espacio + "- expresion: " +  str(expresion)
	retorno += "\n"
	retorno += espacio + espacio + espacio + "- parentesis derecho: " +  "')'"
	return retorno


class ExprAritmetica : 

	# Clase encargada de mostrar la sintaxis de las expresiones aritmeticas
	# en el lenguaje PascalUSB.

	def __init__(self, expresion1,operador=None,expresion2=None):
		self.expresion1 = expresion1
		self.operador = operador
		self.expresion2 = expresion2
		
	def __str__(self):
		espacio ="    "
		retorno = ""
		retorno += espacio + espacio + "- guardia: BIN_ARITMETICA"
		retorno += "\n"
		if( self.expresion1 != "(" ):
			if ( (self.operador != None) and (self.expresion2 != None) ):
				retorno += OperacionBinaria(self.expresion1,self.operador,self.expresion2)
			elif ( (self.operador!= None) and (self.expresion2 == None) ):
				retorno += OperacionUnaria(self.expresion1,self.operador)   
		retorno += "\n"
		return retorno

class ExprRelacional:

	# Clase encargada de mostrar la sintaxis de las expresiones relacionales
	# en el lenguaje PascalUSB.

	def __init__(self, expresion1,operador=None,expresion2=None):
		self.expresion1 = expresion1
		self.operador = operador
		self.expresion2 = expresion2
	def __str__(self):
		espacio ="    "
		retorno = ""
		retorno += espacio + espacio + "- guardia: BIN_RELACIONAL"
		retorno += "\n"
		if( self.expresion1 != "(" ):
			if ( (self.operador != None) and (self.expresion2 != None) ):
				retorno += OperacionBinaria(self.expresion1,self.operador,self.expresion2)
			elif ( (self.operador != None) and (self.expresion2 == None) ):
				retorno += OperacionUnaria(self.expresion1,self.operador)
		return retorno

class ExprBooleana:

	# Clase encargada de mostrar la sintaxis de las expresiones booleanas
	# en el lenguaje PascalUSB.

	def __init__(self,expresion1,operador=None,expresion2=None):
		self.expresion1 = expresion1
		self.operador = operador
		self.expresion2 = expresion2
	def __str__(self):
		espacio ="    "
		retorno = ""
		retorno += "\n"
		retorno += espacio + espacio + "- guardia: BIN_BOOLEANA"
		retorno += "\n"
		if( self.expresion1 != "(" ):
			if ( (self.operador != None) and (self.expresion2 != None) ):
				retorno += OperacionBinaria(self.expresion1,self.operador,self.expresion2)
			elif ( (self.operador != None) and (self.expresion2 == None) ):
				retorno += OperacionUnaria(self.expresion1,self.operador)
			elif ( (self.operador == None) and (self.expresion2 == None) ):
				retorno += espacio + espacio + espacio + espacio + " - variable: " + self.expresion1
		retorno += "\n"
		return retorno

class ExprIntervalo:

	# Clase encargada de mostrar la sintaxis de las expresiones con caracteres
	# en el lenguaje PascalUSB.

	def __init__(self,expresion1,operador,expresion2):
		self.expresion1 = expresion1
		self.operador = operador
		self.expresion2 = expresion2
		
	def __str__(self):
		espacio ="    "
		retorno = ""
		retorno += espacio + espacio + "- guardia: BIN_ARITMETICA"
		retorno += "\n"
		if( self.expresion1 != "(" ):
			if ( (self.operador != None) and (self.expresion2 != None) ):
				retorno += OperacionBinaria(self.expresion1,self.operador,self.expresion2)
			elif ( (self.operador!= None) and (self.expresion2 == None) ):
				retorno += OperacionUnaria(self.expresion1,self.operador)   
		retorno += "\n"
		return retorno


class ExprParentizada:

	# Clase encargada de mostrar la sintaxis de las expresiones parentizadas
	# en el lenguaje PascalUSB.

	def __init__(self,abre,expresion,cierra):
		self.abre = abre
		self.expresion = expresion
		self.cierra = cierra
	def __str__(self):
		espacio = "    "
		retorno = ""
		retorno += "\n"
		retorno += espacio + espacio + "--- Comienza Expresion Parentizada ---"
		retorno += "\n"
		retorno += expresionParentesis(self.expresion)
		retorno += "\n"
		retorno += espacio + espacio + "--- Termina Expresion Parentizada ---"
		retorno += "\n"
		return retorno 

# Clase para definir los tipos.
class Tipo(Expression):

	def __init__(self, typeName):
		self.type = typeName

###############################################################################
#							EXPRESIONES LITERALES		   					  #
###############################################################################

class ExprNumero(Expression):

	# Clase encargada de mostrar la sintaxis de los literales enteros
	# en el lenguaje PascalUSB.

	def __init__ (self,valor):
		self.type = "LITERAL ENTERO"
		self.valor = valor

	def __str__(self):
		espacio = "    "
		retorno = ""
		retorno += "-expresion: " + self.type
		retorno += "\n"
		retorno +=  espacio + espacio + espacio + espacio + "- valor: " + str(self.valor)
		return retorno


class Expr_Booleana(Expression):

	# Clase encargada de mostrar la sintaxis de los literales booleanos
	# en el lenguaje PascalUSB.

	def __init__ (self, Bool):
		self.type = "LITERAL BOOLEANO"
		self.Bool = Bool
		
	def __str__(self):
		espacio = "    "
		retorno = ""
		retorno +=  espacio + " - expresion:" + self.type
		retorno += "\n"
		retorno +=  espacio + espacio + espacio + " - valor: " + str(self.Bool)
		return retorno


class ExprString(Expression):

	# Clase encargada de mostrar la sintaxis de los literales string
	# en el lenguaje PascalUSB.

	def __init__ (self, caracter):
		self.type = "LITERAL STRING"
		self.string = string

	def __str__(self):
		espacio = "    "
		retorno = ""
		retorno += espacio + "- expresion: " + self.type 
		retorno += "\n"
		retorno +=  espacio + espacio + espacio + " - identificador: " + str(self.string)
		return retorno

def Variable (ident):

	# Funcion encargada de mostrar la sintaxis de los literales variables
	# en el lenguaje PascalUSB.

	espacio = "    "
	retorno = ""
	retorno += "VARIABLE" 
	retorno += "\n"
	retorno += espacio+espacio+ espacio + " - identificador: " + str(ident)
	return retorno
	
class PostCondicion(Expression):
	def __init__ (self, expr1, expr2):
		self.type = "POSTCONDICIÓN"
		self.expr1 = expr1
		self.expr2 = expr2
		
	def __str__ (self):
		espacio = "    "
		retorno = ""
		retorno += "\n"
		retorno = espacio + str(self.type)
		retorno += "\n"
		retorno += "\t" + "\t"+ "CONDICIONAL"
		retorno += "\n"
		
		for inst in self.expr1 :
			retorno += str(inst)
			retorno += "\n"

		retorno += "\t" + "\t"
		retorno += "\n"
		for other in self.expr2 :
			retorno += str(other)
			retorno += "\n"
		return retorno
