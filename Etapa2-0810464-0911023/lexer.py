#!/usr/bin/env python
# -*- coding: utf-8 -*-

#######################################
# CI3715 Traductores e Interpretadores
# PascalUSB
# Etapa 1-Lexer
# Fernando González 08-10464
# Honorio Rodríguez 09-11023 
#######################################

import ply.lex as lex

#Palabras reservadas de Pascal-USB
reserved_words = {

    #Estructura de un programa
    'program' : 'TkProgram',
    'begin' : 'TkBegin',
    'end' : 'TkEnd',
    'declare' : 'TkDeclare',
    'as' : 'TkAs',

    #Tipos de datos
    'int' : 'TkInt',
    'bool' : 'TkBool',
    'inter' : 'TkInter',

    #condicionales
    'if' : 'TkIf',
    'then' : 'TkThen',
    'else' : 'TkElse',
    'of' : 'TkOf',
    'case' : 'TkCase',

    #Entrada/Salida
    'read' : 'TkRead',
    'print' : 'TkPrint',
    'println' : 'TkPrintln',

    # Ciclos
    'for': 'TkFor',
    'while': 'TkWhile',
    'do': 'TkDo',
    'in': 'TkIn',

    # Operadores Logicos
    'and': 'TkAnd',
    'or': 'TkOr',
    'not': 'TkNot',

    # Conversiones de tipo y funciones embebidas
    'len' : 'TkLen',
    'itoi' : 'TkItoi',
    'max' : 'TkMax',
    'min' : 'TkMin',

    # Valores Booleanos
    'true' : 'TkTrue',
    'false' : 'TkFalse',

    # Postcondicion
    'Post' : 'TkPost',
    'forall' : 'TkForall',
    'exists' : 'TkExists'
}


tokens = [

    # Identificadores
    'TkId',

    # Cadena de caracteres
    'TkString',

    # Numeros
    'TkNumber',

    # Símbolo utilizados para denotar separadores
    'TkComma',
   # 'TkPoint',
    'TkOpenPar',
    'TkClosePar',
    'TkOpenBra',
    'TkCloseBra',
    'TkTwoPoints',
    'TkPipe',    
    'TkAsig',
    'TkSemicolon',
    'TkArrow',

    # símbolos utilizados para denotar operadores aritméticos,
    #  booleanos, relacionales o de intervalos
    'TkPlus',
    'TkMinus',
    'TkMult',
    'TkDiv',
    'TkMod',
    'TkSoForth',
    'TkCap',
    'TkLess',
    'TkLeq',
    'TkGeq',
    'TkGreater',
    'TkEqual',
    'TkNEqual'

] + list(reserved_words.values())

# Definición de las diferentes expresiones regulares
def t_TkId(t):
    r'([A-Za-z][A-Za-z0-9_]*(?:_+[A-Za-z0-9_]+)*)'
    t.type = reserved_words.get(t.value,'TkId')
    return t


def t_TkString(t):
    r'"([^"\\\n]|\\"|\\\\|\\n)*"'
    t.value = t.value[1:-1]
    return t

def t_TkNumber(t):
    r'(([1-9]\d*|0))+(?![a-zA-Z])'
    t.value = int(t.value)
    return t

t_TkComma = r','
#t_TkPoint = r'\.'
t_TkOpenPar = r'\('
t_TkClosePar = r'\)'
t_TkOpenBra = r'\{'
t_TkCloseBra = r'\}'
t_TkAsig = r':='
t_TkTwoPoints = r':'
t_TkPipe = r'\|'
t_TkSemicolon = r';'
t_TkArrow = r'==>'
t_TkPlus = r'\+'
t_TkMinus = r'-'
t_TkMult = r'\*'
t_TkNEqual = r'/='
t_TkDiv = r'/'
t_TkMod = r'%'
t_TkSoForth = r'\.\.'
t_TkCap = r'<>'
t_TkGreater = r'>'
t_TkLeq = r'<='
t_TkGeq = r'>='
t_TkLess = r'<'
t_TkEqual = r'=='

# Ignora tabulador y comentarios
t_ignore = ' \t'
t_ignore_comments = r'//.*'

# definicion de caracateres no esperados
def t_error(t):
    error.append(t)
    t.lexer.skip(1)

# Encontrar la linea y la columna donde esta el token
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

def find_column(input, token):
    last_cr = input.rfind('\n', 0, token.lexpos)
    if last_cr < 0:
        last_cr = -1
    column = token.lexpos - last_cr
    return column

# lista de errores
global error

error = []

# lista de tokens reconocidos
tokList = []

# Construccioń del lexer
lexer = lex.lex()

# Lectura del archivo
def analyzePUSB (code) :
    
    lexer.input(code)

    while True:
        tok = lexer.token()
        if not tok:
            break
        tokList.append(tok)

    # Impresión de la lista de tokens o errores encontrados
    if (len(error) != 0):
        for obj in error:
            print('Error: Unexpected character "' + str(obj.value[0])\
            + '" in row ' + str(obj.lineno) + ', column ' + \
             str(find_column(lexer.lexdata, obj)))
        exit()     

lex.lex()

if __name__ == '__main__':
	pass    
