#!/usr/bin/env python
# -*- coding: UTF-8 -*-

#########################################
# CI3715 Traductores e Interpretadores  #
# PascalUSB                             #
# Etapa 2-Parser                        #
# Fernando Gonzalez 08-10464            #
# Honorio Rodriguez 09-11023            #
#########################################

import ply.yacc as yacc
from lexer import tokens, find_column, error, tokList, analyzePUSB
from arbol import *

###############################################################################
#								  	PRECEDENCIA				   				  #
###############################################################################

precedence = (
    ('left','TkSemicolon'),
    ('left','TkMinus'),
    ('left','TkOpenBra','TkCloseBra'),
    ('left','TkOpenPar','TkClosePar'),
    ('left','TkSoForth'),
    ('left','TkMult','TkDiv','TkMod'),
    ('left','TkPlus'),
    ('left','TkCap'),
    ('left','TkLess','TkLeq','TkGeq','TkGreater'),
    ('left','TkEqual','TkNEqual'),
    ('left','TkIn'),
    ('left','TkNot'),
    ('left','TkAnd','TkOr')
)

###############################################################################
#									INICIO				   					  #
###############################################################################

def p_S(p):
    '''s : TkProgram bloque'''
    if len(p) == 3 :
        p[0] = p[2]

def p_bloque(p):
    '''bloque : TkBegin TkDeclare listDeclare listInstr TkEnd postCondicion
                | TkBegin TkDeclare listDeclare listInstr TkEnd
                | TkBegin listInstr TkEnd postCondicion      
                | TkBegin listInstr TkEnd %prec TkSemicolon'''
    if len(p) == 7 :
        p[0] = Bloque(Declare=p[3], Begin=p[4], PostCondicion=p[6])
    elif len(p) == 6 :
        p[0] = Bloque(Declare=p[3], Begin=p[4], PostCondicion = None)
    elif len(p) == 5 :
        p[0] = Bloque(Declare= None, Begin=p[2], PostCondicion=p[4])
    elif len(p) == 4 :
        p[0] = Bloque(Declare= None, Begin=p[2], PostCondicion= None)           

###############################################################################
#							DECLARACIONES				   					  #
###############################################################################

def p_listDeclare(p):
    '''listDeclare : listId TkAs listTipos TkSemicolon
                    | listId TkAs listTipos'''
    if len(p) == 5 :
        p[0] = p[1]
        p[0] + p[3]
    elif len(p) == 4 :
        p[0] = p[1]
        p[0] + p[3]

def p_listTipos(p):
    '''listTipos : tipo TkComma listTipos
                | tipo'''
    if len(p) == 4 :
        p[0] = p[1]
    elif len(p) == 2:
        p[0] = p[1] 

def  p_tipo(p):
    '''tipo : TkInt
            | TkBool
            | TkInter'''                        
    p[0] = p[1]          

def p_listId(p) :
    '''listId : TkId TkComma listId
                | TkId'''
    if len(p) == 4 :
        p[0] = p[1]
        p[0] + p[3]
    elif len(p) == 2:
        p[0] = []
        p[0] = p[1]                                            

###############################################################################
#							INSTRUCCIONES				   					  #
###############################################################################

def p_listInstr(p):
    '''listInstr : bloque
                    | instruc'''
    p[0] = p[1]

def p_instruc(p):
    '''instruc : asignacion
                | read
                | print
                | condIf
                | condCase
                | iteraFor
                | iteraWhile'''
    p[0] = p[1]        

def p_asignacion(p):
    '''asignacion : TkId TkAsig expresion TkSemicolon'''
    p[0] = Asignacion(p[1],p[3])    

def p_read(p):
    '''read : TkRead TkId'''
    p[0] = Read(p[2])

def p_print(p):
    '''print : TkPrint expresion %prec TkSemicolon
              | TkPrintln expresion %prec TkSemicolon'''
    if (p[1] == 'print'):
        p[0] = Print(p[2])
    elif (p[1] == 'println'):
        p[0] = Println(p[2])       

def p_condIf(p):
    '''condIf : TkIf expresion TkThen listInstr TkElse listInstr %prec TkSemicolon
                | TkIf expresion TkThen listInstr %prec TkSemicolon'''
    if len(p) == 7:
        p[0] = Condicional(expr=p[2],listCondIf=p[4],listCondElse=p[6])
    elif len(p) == 5:    
        p[0] = Condicional(expr=p[2],listCondIf=p[4])

def p_condCase(p):
    '''condCase : TkCase expresion TkOf listCase TkEnd %prec TkSemicolon'''
    if len(p) == 6:
        p[0] = Case(expr=p[2],listCase=p[4]) 

def p_listCase(p):
    '''listCase : expresion TkArrow listInstr listCase 
                | expresion TkArrow listInstr'''
    if len(p) == 5:
        p[0] = p[1]
        p[0] + p[3]
        p[0] + p[4]
    elif len(p) == 4:
        p[0] = p[1]
        p[0] + p[3]            

def p_iteraWhile(p):
    '''iteraWhile : TkWhile expresion TkDo listInstr %prec TkSemicolon'''
    if (len(p) == 6) :
        p[0] = While(p[2],p[4])

def p_iteraFor(p):
    '''iteraFor : TkFor TkId TkIn expresion TkDo listInstr %prec TkSemicolon'''
    if (len(p) == 7) :
        p[0] = For(ident=p[2],paso=p[4],instr=p[6])

###############################################################################
#							EXPRESIONES				   					      #
###############################################################################

def p_expresiones(p):
    '''expresion : TkOpenPar expresion TkClosePar
                    | expresion TkPlus expresion
                    | expresion TkMinus expresion
                    | expresion TkMult expresion
                    | expresion TkDiv expresion
                    | expresion TkMod expresion
                    | expresion TkSoForth expresion
                    | expresion TkCap expresion
                    | expresion TkLess expresion
                    | expresion TkLeq expresion
                    | expresion TkGeq expresion
                    | expresion TkGreater expresion
                    | expresion TkEqual expresion
                    | expresion TkNEqual expresion
                    | TkNot expresion
                    | TkMinus expresion                    
                    | expresion TkAnd expresion
                    | expresion TkOr expresion
                    | expresion TkIn expresion
                    | TkLen TkOpenPar expresion TkClosePar
                    | TkItoi TkOpenPar expresion TkClosePar
                    | TkMax TkOpenPar expresion TkClosePar
                    | TkMin TkOpenPar expresion TkClosePar
                    | bool
                    | TkId
                    | string
                    | numero''' 

    # Operadores Binarios
    if len(p) == 4:
        if (p[2]=='+' or p[2]=='-' or p[2]=='*' or p[2]=='/' or p[2]=='%'):
            p[0] = ExprAritmetica(p[1],p[2],p[3])
        elif (p[2]=='and' or p[2]=='or'):
            p[0] = ExprBooleana(p[1],p[2],p[3])
        elif (p[2]=='<'or p[2]=='<=' or p[2]=='>' or p[2]=='>=' or p[2]=='==' or p[2]=='/=' or p[2]=='in'): 
            p[0] = ExprRelacional(p[1],p[2],p[3])
        elif ((p[1] == '(') and (p[3] == ')')):    
            p[0] = ExprParentizada(p[1],p[2],p[3])
        elif (p[2]=='..' or p[2]=='<>'):
            p[0] = ExprIntervalo(p[1],p[2],p[3])    

    # Operadores Unarios
    elif len(p) == 3 :
        if (p[1]=='not'):
            p[0] = ExprBooleana(expresion=p[2],operador=p[1])
        elif (p[1]=='-'):
            p[0] = ExprAritmetica(expresion=p[2],operador=p[1])

    # Funciones embebidas
    elif len(p) == 5 :
        if (p[1]=='itoi' or p[1]=='len' or p[1]=='max' or p[1]=='min'):
            p[0] = FuncEmbebida(funcion=p[1],expr=p[3])

    # Simbolos terminales
    elif len(p) == 2:
        p[0] = p[1]    

def p_bool(p):
    '''bool : TkTrue
            | TkFalse'''
    p[0] = Expr_Booleana(p[1])

def p_numero(p):
    '''numero : TkNumber'''
    p[0] = ExprNumero(p[1])

def p_string(p):
    '''string : TkString'''
    p[0] = ExprString(p[1])    

###############################################################################
#							POSTCONDICION				   					      #
###############################################################################

def p_postCondicion(p):
    '''postCondicion : TkOpenBra TkPost TkTwoPoints TkOpenPar listPost TkClosePar TkCloseBra'''
    p[0] = p[5]

def p_listPost(p):
    '''listPost : TkForall TkId TkPipe TkId TkIn expresion TkTwoPoints expresion
                    | TkExists  TkId TkPipe TkId TkIn expresion TkTwoPoints expresion'''
    p[0] = PostCondicion(p[5],p[7])


###############################################################################
#								MANEJO DE ERRORES			   				  #
###############################################################################

def p_error(p):
    if p is not None:
        print '\n'
        print("Sintax error in row %s, column %s: unexpected token '%s'"%(
                                                        p.lexer.lineno,
                                                        find_column(p.lexer.lexdata,p),
                                                        p.value
                                                        )
        )
        exit()
    else:
        print("Error de sintaxis")
        print '\n'    
        exit()

###############################################################################
#							CONSTRUYE EL PARSER				   				  #
###############################################################################

def build_parser(code):
    parser = yacc.yacc(debug=True)
    parser.error = 0
    arbol = parser.parse(code)
    if parser.error:
        ast = None
    return arbol  
  